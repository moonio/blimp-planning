var gulp = require('gulp'),
    less = require('gulp-less'),
    webserver = require('gulp-webserver'),
    autoprefixer = require('gulp-autoprefixer'),
    developmentRoot = 'development';

    gulp.task('webserver', function() {
      gulp.src(developmentRoot)
        .pipe(webserver({
          livereload: true,
          directoryListing: false,
          open: true
        }));
    });

    gulp.task('less', function () {
        gulp.src('less/**/*.less')
            .pipe(less())
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(gulp.dest(developmentRoot+'/css'));
    });

    gulp.task('watch-less', function(){
        gulp.watch('less/**/*.less', function(){
            gulp.run('less');
        });
    });

    gulp.task('dev', function () {
        gulp.run('webserver');
        gulp.run('watch-less');
    });